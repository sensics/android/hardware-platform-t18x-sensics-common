
import dsi

def initSequence(vfp=50, vbp=750, vsw=4, fps=60):
    vbpsw = vbp + vsw
    vbpswHigh, vbpswLow = dsi.toHighLow(vbpsw)
    vfpHigh, vfpLow = dsi.toHighLow(vfp)
    return dsi.generateProperties("nvidia,dsi-init-cmd", [
        # Panel init sequence
        dsi.dcsWrite1Param("0xff", "0x20"),
        dsi.dcsWrite1Param("0xfb", "0x01"),
        dsi.dcsWrite1Param("0x5d", "0x0f"),

        dsi.delayMs(1),
        dsi.dcsWrite1Param("0xff", "0xe0"),
        dsi.dcsWrite1Param("0xfb", "0x01"),
        dsi.dcsWrite1Param("0x53", "0x22"),

        dsi.delayMs(1),
        dsi.dcsWrite1Param("0xff", "0x25"),
        dsi.dcsWrite1Param("0xfb", "0x01"),
        dsi.dcsWrite1Param("0xc4", "0x10"),
        dsi.dcsWrite1Param("0x62", "0x60"),
        dsi.dcsWrite1Param("0x66", "0x40"),
        dsi.dcsWrite1Param("0x67", "0x3c"),

        dsi.delayMs(1),
        dsi.dcsWrite1Param("0xff", "0x10"),
        dsi.dcsWrite1Param("0xfb", "0x01"),
        dsi.dcsWrite1Param("0xc0", "0x80"),

        dsi.delayMs(1),
        dsi.dcsWrite1Param("0xba", "0x07"), # Enable only port A

        dsi.delayMs(1),
        # Set up the porches used in high-frequency-mode
        dsi.genericLongWrite(["0xbe",
            dsi.makeHex(vbpswHigh), dsi.makeHex(vbpswLow),
            dsi.makeHex(vfpHigh), dsi.makeHex(vfpLow)]),

        dsi.delayMs(1),
        dsi.dcsWrite1Param("0xbb", "0x03"),
        dsi.dcsWrite1Param("0x35", "0x00"),
        dsi.dcsWrite1Param("0x36", "0x00"),

        # Init commands post video transmit (from standalone firmware)
        dsi.delayMs(100),
        dsi.genericWrite1Param("0xb0", "0x00"), # MCAP
        dsi.delayMs(1),
        dsi.genericWrite1Param("0xb3", "0x14"), # Interface Setting
        dsi.delayMs(1),
        dsi.genericWrite1Param("0xb0", "0x03"), # MCAP

        dsi.delayMs(1),

        # Display on
        dsi.dcsWrite0Param("DSI_DCS_EXIT_SLEEP_MODE"),
        dsi.delayMs(200),
        dsi.dcsWrite0Param("DSI_DCS_SET_DISPLAY_ON"),
        dsi.delayMs(200)
    ]) + "\n" + dsi.makePropertyCell("nvidia,dsi-refresh-rate", fps)