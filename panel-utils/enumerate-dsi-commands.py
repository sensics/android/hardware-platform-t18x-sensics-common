#!/usr/bin/env python

cmd_types = [
    "TEGRA_DSI_PACKET_CMD",
    "TEGRA_DSI_PACKET_VIDEO_VBLANK_CMD",
    "TEGRA_DSI_DELAY_MS",
    "TEGRA_DSI_SEND_FRAME",
    "TEGRA_DSI_GPIO_SET"
]

data_ids = [
    "DSI_GENERIC_LONG_WRITE",
    "DSI_DCS_LONG_WRITE",
    "DSI_GENERIC_SHORT_WRITE_1_PARAMS",
    "DSI_GENERIC_SHORT_WRITE_2_PARAMS",
    "DSI_DCS_WRITE_0_PARAM",
    "DSI_DCS_WRITE_1_PARAM",
    #"DSI_DCS_SET_ADDR_MODE",
    #"DSI_DCS_EXIT_SLEEP_MODE",
    #"DSI_DCS_ENTER_SLEEP_MODE",
    #"DSI_DCS_SET_DISPLAY_ON",
    #"DSI_DCS_SET_DISPLAY_OFF",
    #"DSI_DCS_SET_TEARING_EFFECT_OFF",
    #"DSI_DCS_SET_TEARING_EFFECT_ON",
    #"DSI_DCS_NO_OP",
    "DSI_NULL_PKT_NO_DATA",
    "DSI_BLANKING_PKT_NO_DATA"
]

def is_dsi_cmd(cmd_type):
    return cmd_type in [
        "TEGRA_DSI_PACKET_CMD",
        "TEGRA_DSI_PACKET_VIDEO_VBLANK_CMD"
    ]
def is_long_pkt(data_id):
    return data_id in [
        "DSI_GENERIC_LONG_WRITE",
        "DSI_DCS_LONG_WRITE",
        "DSI_NULL_PKT_NO_DATA",
        "DSI_BLANKING_PKT_NO_DATA"
    ]
ARG1_PLACEHOLDER = "[arg1]"
ARG2_PLACEHOLDER = "[arg2]"
ARG3_PLACEHOLDER = "[arg3]"
ECC_PLACEHOLDER = "[ecc-ignored]"

def replace_placeholder(my_list, placeholder, newVal):
    return [newVal if x==placeholder else x for x in my_list]
def make_cmd_cell(cmd_type, data_id):
    return [cmd_type, data_id, ARG1_PLACEHOLDER, ARG2_PLACEHOLDER, ECC_PLACEHOLDER]
def generate_entries():
    for cmd_type in cmd_types:
        # this is the first value
        if is_dsi_cmd(cmd_type):
            for data_id in data_ids:
                # this is the second value
                # we've read 2 more values and discarded another
                cell = [cmd_type, data_id, ARG1_PLACEHOLDER, ARG2_PLACEHOLDER, ECC_PLACEHOLDER]
                if cmd_type=="TEGRA_DSI_PACKET_VIDEO_VBLANK_CMD" and not is_long_pkt(data_id):
                    cell.append(ARG3_PLACEHOLDER)
                if is_long_pkt(data_id):
                    cell = replace_placeholder(cell, ARG2_PLACEHOLDER, "[sp_len_dly.data_len MSB:u8]")
                    cell = replace_placeholder(cell, ARG1_PLACEHOLDER, "[sp_len_dly.data_len LSB:u8]")
                    cell.append("[pdata payload - sp_len_dly.data_len bytes]")
                    cell.append(ECC_PLACEHOLDER)
                    cell.append(ECC_PLACEHOLDER)
                    # -1 because the payload could be empty
                    size = ">={0}".format(len(cell) - 1)
                    yield size, cell
                else:
                    cell = replace_placeholder(cell, ARG1_PLACEHOLDER, "[sp_len_dly.sp.data0]")
                    cell = replace_placeholder(cell, ARG2_PLACEHOLDER, "[sp_len_dly.sp.data1]")
                    if cmd_type == "TEGRA_DSI_PACKET_VIDEO_VBLANK_CMD":
                        cell = replace_placeholder(cell, ARG3_PLACEHOLDER, "[club_cmd:bool]")
                    yield len(cell), cell
        elif cmd_type == "TEGRA_DSI_DELAY_MS":
            yield 2, [cmd_type, "[sp_len_dly.delay_ms:u16]"]
        elif cmd_type == "TEGRA_DSI_SEND_FRAME":
            yield 2, [cmd_type, "[sp_len_dly.frame_cnt:u16]"]
        elif cmd_type == "TEGRA_DSI_SEND_FRAME":
            yield 3, [cmd_type, "[sp_len_dly.gpio]", "[data]"]

for cells, cell_template in generate_entries():
    #print("<{0}> ({1} cells)".format("  ".join(cell_template), cells))
    print("{1} cells,{0}".format(",".join(cell_template), cells))

