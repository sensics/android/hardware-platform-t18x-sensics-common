

def toHighLow(uint16):
    #import struct
    #return struct.unpack("=BB", struct.pack(">H", uint16))
    return (uint16 >> 8) & 255, uint16 & 255

def makeHex(val):
    return "{:#02x}".format(val)

def makePropertyCell(name, val):
    return "{0} = <{1}>;".format(name, val)

def generateProperties(propName="nvidia,dsi-init-cmd", entries=[]):
    countPropName = propName.replace("dsi-", "dsi-n-")
    return "\n".join([
        "{0} = {1};".format(propName, ",\n		".join(entries)),
        makePropertyCell(countPropName, len(entries))])


def dcsWrite1Param(cmd, param):
    return "<SENSICS_INIT_CMD DSI_DCS_WRITE_1_PARAM {0} {1} 0x0 SENSICS_CLUBBED>".format(cmd, param)

def dcsWrite0Param(cmd):
    return "<SENSICS_INIT_CMD DSI_DCS_WRITE_0_PARAM {0} 0x0 0x0 SENSICS_CLUBBED>".format(cmd)

def genericWrite1Param(cmd, param):
    return "<SENSICS_INIT_CMD DSI_GENERIC_SHORT_WRITE_1_PARAMS {0} {1} 0x0 SENSICS_CLUBBED>".format(cmd, param)

def delayMs(ms):
    return "<TEGRA_DSI_DELAY_MS {0}>".format(ms)


def genericLongWrite(bytes):
    (sizeHigh, sizeLow) = toHighLow(len(bytes))
    allArgs = [makeHex(sizeLow), makeHex(sizeHigh), "0x0"] # last byte is ecc - ignored
    allArgs.extend(bytes)
    allArgs.extend(["0x0", "0x0"]) #ecc - ignored
    return "<SENSICS_INIT_CMD DSI_GENERIC_LONG_WRITE {0}>".format(" ".join(allArgs))
